# django-checkmigrations

manage.py command to check if all migrations are applied

## Project Requirements
* After adding app to `INSTALLED_APPS` user 'll be able to run `./manage.py checkmigrations`
* Command has next flags:
    * Required
        * `-h` `--help` show this message
        * `-d <db_name>` `--database <db_name>` specifying database for check
        * ~~`-v <0,1,2,3>` `--verbose <0,1,2,3>`~~
            * ~~0 - return exit code w/o printing anything (_Can be useless without `-F` flag_)~~
            * ~~1 - print message with count of unapplied migrations~~
            * ~~2 - print messages about checking every migration~~
            * ~~3 - debug mode~~
        * `-v` `--verbose` print message on every migration check
        * `-s` `--silent` return exit code w/o printing anything (_Can be useless without `-F` flag_)
        * `-F` `--force-exit-code` return exit code 1 if found unapplied migrations
        * `-u` `--show-unapplied` return list of unapplied migrations
        * `-O` `--show-over` return list of overapplied migration
        * `-A` `--show-all` return list of all migrations
    * Nice to have
        * `-p <abs,rel, base>` `--show-pass <abs,rel, base>` if `-u` or `-a` flags specified will print path to every migration file
            * abs (_default value_) - print absolute path to every migration file
            * rel - print relative path to every migration file starting from script dir
            * base - print relative path to every migration file starting from `BASE_DIR`
        * `--colored <[true,yes,1], [false,no,0]>` define if output must be colorful or not
            * true - use colors
            * false - one-color output
* Command compatitible with old version of django and python.
    * Django:
        * 2.2
        * 3.0
        * 3.1
    * Python:
        * 3.6
        * 3.7
        * 3.8
        * 3.9
        * 3.10
* Command tested on all envs with Tox or similar
* Type hits added
* MyPy, Flake8 checks passed
* Can be easilly installed fron PyPi
* Docs added
* Pipeline on every master push
